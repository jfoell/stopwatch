/*
 * Copyright (c) 2023 Jacob Foell <jfoell@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

static int stopped = 0;

static void
sig_handler(int signo)
{
	/* NOTE: The only signal registered is SIGINT. */
	stopped = 1;
}

static int
clamp(int value, int max)
{
	int clamped = value;

	if (value > max)
	{
		clamped = max;
	}

	return clamped;
}

static int
puthours(int hours)
{
	hours = clamp(hours, 99);
	printf("%02d", hours);

	return 0;
}

static int
putmins(int mins)
{
	mins = clamp(mins, 59);
	printf("%02d", mins);

	return 0;
}

static int
putsecs(int secs)
{
	secs = clamp(secs, 59);
	printf("%02d", secs);

	return 0;
}

static int
putmsecs(int msecs)
{
	msecs = clamp(msecs, 999);
	printf("%03d", msecs);

	return 0;
}

static int
stopwatch_print(struct timespec *elapsed)
{
	/* format: HH:MM:SS.mmm */
	int const sec_per_hr = (60 * 60);
	int const sec_per_min = 60;
	int secs = (int)elapsed->tv_sec;

	int hours = secs / sec_per_hr;
	secs -= (hours * sec_per_hr);
	int mins = secs / sec_per_min;
	secs -= (mins * sec_per_min);
	int ms = ((elapsed->tv_nsec / 100000) + 5) / 10;

	putchar('\r');

	puthours(hours);
	putchar(':');
	putmins(mins);
	putchar(':');
	putsecs(secs);
	putchar('.');
	putmsecs(ms);

	return 0;
}

static int
stopwatch_cursor_disable(void)
{
	/* Terminal escape sequence magic. */
	printf("\e[?25l");

	return 0;
}

static int
stopwatch_cursor_enable(void)
{
	/* Terminal escape sequence magic. */
	printf("\e[?25h");

	return 0;
}

static int
stopwatch_diff_calc(struct timespec *b, struct timespec *a, struct timespec *e)
{
	if ((b->tv_sec < a->tv_sec) ||
		((b->tv_sec == a->tv_sec) && (b->tv_nsec < a->tv_nsec)))
	{
		e->tv_sec = 0;
		e->tv_nsec = 0;
	}
	else
	{
		time_t sec_diff = 0;
		long nsec_diff = 0;

		sec_diff = b->tv_sec - a->tv_sec;
		if (b->tv_nsec > a->tv_nsec)
		{
			nsec_diff = b->tv_nsec - a->tv_nsec;
		}
		else
		{
			nsec_diff = b->tv_nsec + (1000000000 - a->tv_nsec);
		}

		e->tv_sec = sec_diff;
		e->tv_nsec = nsec_diff;
	}

	return 0;
}

int
main(int argc, char **argv)
{
	struct timespec origin;

	clock_gettime(CLOCK_MONOTONIC, &origin);

	signal(SIGINT, sig_handler);
	stopwatch_cursor_disable();
	while (!stopped)
	{
		struct timespec now;
		struct timespec elapsed;

		clock_gettime(CLOCK_MONOTONIC, &now);
		stopwatch_diff_calc(&now, &origin, &elapsed);
		stopwatch_print(&elapsed);
		fflush(stdout);
		usleep(50000);
	}
	stopwatch_cursor_enable();
	putchar('\n');

	return 0;
}

