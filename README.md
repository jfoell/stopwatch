# stopwatch

## Name
`stopwatch`: a simple c program for a medium-resolution terminal stopwatch.
Useful to perform basic timing activities, or use it to learn and study a simple
c program.

## Description
This is a simple terminal application to provide a stopwatch.

## Build
`gcc -Wall -Ofast main.c -o stopwatch && strip -s stopwatch`

## Installation
Move the `stopwatch` file to a directory in your `$PATH`
example: `mv stopwatch $HOME/bin`

## Usage
At any terminal type `stopwatch`
...to stop: `Ctrl-C`

## Contributing
Lets fix any bugs you find! Feel free to fork it for your own purposes or
suggest branches to create a `stopwatch` with more features.

## Authors and acknowledgment
Jacob Foell <jfoell@gmail.com>

## License
MIT License

## Project status
It is complete.

